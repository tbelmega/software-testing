package de.tbelmega.lab2;

/**
 * @author <put your name here>
 * Class for shipping containers
 */
public class ShippingContainer {

	public static final int INVALID_SIZE=-1;
	public static final int NO_CONTAINER_AVAILABLE=0;
	public static final int SMALL_CONTAINER=1;
	public static final int MEDIUM_CONTAINER=2;
	public static final int LARGE_CONTAINER=3;

	/**
	 * @param size - the size of the load in cubic metres
	 * @return The correct container for this size load:<br>
	 *         - SMALL_CONTAINER for 1 to 5 cubic-metres<br>
	 *         - MEDIUM_CONTAINER for 6 to 10 cubic metres<br>
	 *         - LARGE_CONTAINER for 11 to 30 submic metres<br>
	 *         - or NO_CONTAINER_AVAILABLE if too large for any container<br>
	 *         - or INVALID_INPUTS if the size is less than 1 cubic metre
	 */
	public static int getContainer( int size ) {
		int selected = NO_CONTAINER_AVAILABLE;
		if (size < 1)
		   selected = INVALID_SIZE;
		else if (size>30)
			selected = NO_CONTAINER_AVAILABLE;
		else if (size<6)
			selected = SMALL_CONTAINER;
		else if (size<11)
			selected = MEDIUM_CONTAINER;
		else if (size<21)
			selected = MEDIUM_CONTAINER;
		else if (size<=30)
			selected = LARGE_CONTAINER;
		else
			selected = NO_CONTAINER_AVAILABLE;
		return selected;
	}

}