package de.tbelmega.lab2;

import org.testng.ITestResult;
import org.testng.TestListenerAdapter;
import org.testng.TestNG;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by Thiemo on 04.03.2016.
 */
public class CustomRunner {
    public static void main(String args[])
            throws ClassNotFoundException, FileNotFoundException {
        PrintStream log=
                new PrintStream(new FileOutputStream("test.log", true));
        TestNG testng=new TestNG();
        TestListenerAdapter listener=new TestListenerAdapter();
        int i=0;
        Class<?> classList[]=new Class[args.length];
        // Check inputs
        if (args.length==0)
            System.err.println("Error: no classes specified");
        else {
            // Set the test classes to use
            for (String s: args) classList[i++] = Class.forName(s);
            testng.setTestClasses( classList );
            testng.addListener( listener );
            // Run the tests
            testng.run();
            // Print the results to log file
            List<ITestResult> failedTests =  listener.getFailedTests();
            List<ITestResult> passedTests =  listener.getPassedTests();
            List<ITestResult> skippedTests =  listener.getSkippedTests();
            log.println("Test Results");
            log.println("   Date of test: "+new Date().toString());
            log.println("   Test classes: "+ Arrays.toString(args));
            log.println("   Tests passed: "+passedTests.size());
            log.println("   Tests skipped: "+skippedTests.size());
            log.println("   Tests failed: "+failedTests.size());
            for (ITestResult result: failedTests) {
                log.println("      "+result.getName()+" failed, "+
                        result.getThrowable().getMessage());
            }
        }
    }
}
