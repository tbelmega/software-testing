package de.tbelmega.lab2;

import static org.testng.AssertJUnit.assertEquals;
import org.testng.annotations.Test;

public class ShippingContainerTest {

	/**
	 * Test for invalid container size
	 */
	@Test
	public void test1() {
		assertEquals( ShippingContainer.INVALID_SIZE, ShippingContainer.getContainer(-10) );
	}
	/**
	 * Test for small container
	 */
	@Test
	public void test2() {
		assertEquals( ShippingContainer.SMALL_CONTAINER, ShippingContainer.getContainer(3) );
	}
	/**
	 * Test for medium container
	 */
	@Test
	public void test3() {
		assertEquals( ShippingContainer.MEDIUM_CONTAINER, ShippingContainer.getContainer(7) );
	}
	/**
	 * Test for large container
	 */
	@Test
	public void test4() {
		assertEquals( ShippingContainer.LARGE_CONTAINER, ShippingContainer.getContainer(20) );
	}
	/**
	 * Test for outsize load
	 */
	@Test
	public void test5() {
		assertEquals( ShippingContainer.NO_CONTAINER_AVAILABLE, ShippingContainer.getContainer(50) );
	}

}