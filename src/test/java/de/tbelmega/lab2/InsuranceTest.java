package de.tbelmega.lab2;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;

public class InsuranceTest {


  @Test(dataProvider=InsuranceTestDataProvider.TESTS_LAB_2, dataProviderClass = InsuranceTestDataProvider.class)
  public void testThatPremiumIsCalculatedAsExpected(
          String testName, int expectedPremium , int age, char gender, boolean married) {
      //act
      int resultPremium = Insurance.premium( age, gender, married );

      //assert
      assertEquals(expectedPremium, resultPremium);
  }


}
