package de.tbelmega.lab2;
import org.testng.annotations.DataProvider;


/**
 * Created by Thiemo on 18.02.2016.
 */
public class InsuranceTestDataProvider {

    public static final String TESTS_LAB_2 = "provideCarInsurancePremiumData";
    private static final String TESTS_LAB_4 = "provideCarInsurancePremiumDataForLab4";

    @DataProvider(name= TESTS_LAB_2)
    public static Object[][] provideCarInsurancePremiumData() {
        return new Object[][] {
                { "EP_1",  0, 10, 'F', true },
                { "EP_2", 2000, 20, 'M', false },
                { "EP_3", 500, 30, 'M', false },
                { "EP_4", 400, 50, 'M', false },
                { "EP_5", 0, 200, 'M', false },
                { "EP_6", 0, -5, 'F', true }
        };
    }

    @DataProvider(name= TESTS_LAB_4)
    public static Object[][] provideCarInsurancePremiumData4() {
        return new Object[][] {
                { "EP_1",  0, 10, 'F', true },
                { "EP_2", 2000, 20, 'M', false },
                { "EP_3", 500, 30, 'M', false },
                { "EP_4", 400, 50, 'M', false },
                { "EP_5", 0, 200, 'M', false },
                { "EP_6", 0, -5, 'F', true }
        };
    }



}
