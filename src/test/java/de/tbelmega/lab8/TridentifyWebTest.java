package de.tbelmega.lab8;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;

import static org.testng.Assert.assertNotEquals;
import static org.testng.AssertJUnit.*;

public class TridentifyWebTest {

    public static final String IDENTIFY_BUTTON_NAME = "identify-button";
    public static final String CONTINUE_BUTTON_NAME = "Continue-button";
    public static final String RESULT_FIELD_NAME = "Type";
    public static final String TYPE_ATTRIBUTE = "type";
    public static final String INPUT_TAG = "input";
    public static final String SUBMIT_TYPE = "submit";
    public static final String VALUE = "value";
    public static final String READONLY_ATTRIBUTE = "readonly";

    public static final String ISOSCELES = "Isosceles";
    public static final String EQUILATERAL = "Equilateral";
    public static final String SCALENE = "Scalene";
    public static final String INVALID_TRIANGLE = "Invalid Triangle";
    public static final String INVALID_INPUT = "Invalid Input";

    static WebDriver driver;
    static Wait<WebDriver> wait;
    static int delay=0; // optional delay so you can watch progress
	static String url="http://webcourse.cs.nuim.ie/~sbrown/cs608/tridentifyp.html";
	
	WebElement field1, field2, field3, identifyButton;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
        // setup selenium test
        driver = new FirefoxDriver();
        wait = new WebDriverWait(driver, 30);
        driver.get(url);
    	// wait until displayed
    	(new WebDriverWait(driver, 10000))
    			  .until(ExpectedConditions.presenceOfElementLocated(By.name(IDENTIFY_BUTTON_NAME)));
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		driver.close();
	}

	@BeforeMethod
	public void setUp() throws Exception {
		assert isOnInputPage();
    	// find input fields
    	field1 = driver.findElement(By.name("side1"));
    	field2 = driver.findElement(By.name("side2"));
    	field3 = driver.findElement(By.name("side3"));
    	identifyButton = driver.findElement(By.name(IDENTIFY_BUTTON_NAME));
	}

	// Always leave at main screen after test complete
	@AfterMethod
	public void tearDown() throws Exception {
        if (isOnResultsPage()) clickContinueButtonAndDelay();
    	// click - note this waits for the new page to load & all references are now stale!
    	// wait until displayed
    	(new WebDriverWait(driver, 10000))
    			  .until(ExpectedConditions.presenceOfElementLocated(By.name(IDENTIFY_BUTTON_NAME)));
	}

    @Test
    public void componentTestField1() throws Exception {
        field1.sendKeys("1234");
        assertEquals("1234", field1.getAttribute(VALUE));
        assertTrue(field1.isDisplayed());
        assertTrue(field1.isEnabled());
    }

    @Test
    public void componentTestField2() throws Exception {
        field2.sendKeys("1234");
        assertEquals("1234", field2.getAttribute(VALUE));
        assertTrue(field1.isDisplayed());
        assertTrue(field1.isEnabled());
    }

    @Test
    public void componentTestField3() throws Exception {
        field3.sendKeys("1234");
        assertEquals("1234", field3.getAttribute(VALUE));
        assertTrue(field1.isDisplayed());
        assertTrue(field1.isEnabled());
    }

    @Test
    public void componentTestButtonIdentify() throws Exception {
        assertEquals(INPUT_TAG, identifyButton.getTagName());
        assertEquals(SUBMIT_TYPE, identifyButton.getAttribute(TYPE_ATTRIBUTE));
        assertTrue(identifyButton.isDisplayed());
        assertTrue(identifyButton.isEnabled());
    }

    @Test
    public void componentTestButtonContinue() throws Exception {
        clickIdentifyButtonAndDelay();
        WebElement continueButton = driver.findElement(By.name(CONTINUE_BUTTON_NAME));

        assertEquals(INPUT_TAG, continueButton.getTagName());
        assertEquals(SUBMIT_TYPE, continueButton.getAttribute(TYPE_ATTRIBUTE));
        assertTrue(continueButton.isDisplayed());
        assertTrue(continueButton.isEnabled());
    }

    @Test
    public void componentTestResultField() throws Exception {
        clickIdentifyButtonAndDelay();
        WebElement resultField = driver.findElement(By.name(RESULT_FIELD_NAME));

        resultField.sendKeys("1234");
        //read-only. should not take the value
        assertNotEquals("1234", resultField.getAttribute(VALUE));
        assertEquals(Boolean.TRUE.toString(), resultField.getAttribute(READONLY_ATTRIBUTE));

        assertTrue(resultField.isDisplayed());
        assertTrue(resultField.isEnabled());
    }


	@Test
	public void navigationTest1() throws Exception {
	    //Starting on input page
        clickIdentifyButtonAndDelay();

	    //Should land on result page
        assertTrue(isOnResultsPage());
    }

    @Test
	public void navigationTest2() throws Exception {
	    //Navigate to result page and click continue
        clickIdentifyButtonAndDelay();
        clickContinueButtonAndDelay();

	    //Should land on input page
        assertTrue(isOnInputPage());
    }

    @Test
    public void featureTestIsosceles() throws Exception {
        //arrange

        //act
        field1.sendKeys("10");
        field2.sendKeys("10");
        field3.sendKeys("10");
        clickIdentifyButtonAndDelay();
        String result = driver.findElement(By.name(RESULT_FIELD_NAME)).getAttribute(VALUE);

        //assert
        assertEquals(ISOSCELES, result);
    }

    @Test
    public void featureTestEquilateral() throws Exception {
        //arrange

        //act
        field1.sendKeys("20");
        field2.sendKeys("10");
        field3.sendKeys("10");
        clickIdentifyButtonAndDelay();
        String result = driver.findElement(By.name(RESULT_FIELD_NAME)).getAttribute(VALUE);

        //assert
        assertEquals(EQUILATERAL, result);
    }

    @Test
    public void featureTestScalene() throws Exception {
        //arrange

        //act
        field1.sendKeys("30");
        field2.sendKeys("25");
        field3.sendKeys("10");
        clickIdentifyButtonAndDelay();
        String result = driver.findElement(By.name(RESULT_FIELD_NAME)).getAttribute(VALUE);

        //assert
        assertEquals(SCALENE, result);
    }

    @Test
    public void featureTestInvalidTriangle() throws Exception {
        //arrange

        //act
        field1.sendKeys("30");
        field2.sendKeys("5");
        field3.sendKeys("10");
        clickIdentifyButtonAndDelay();
        String result = driver.findElement(By.name(RESULT_FIELD_NAME)).getAttribute(VALUE);

        //assert
        assertEquals(INVALID_TRIANGLE, result);
    }

    @Test
    public void featureTestInvalidInput() throws Exception {
        //arrange

        //act
        field1.sendKeys("");
        field2.sendKeys("5");
        field3.sendKeys("10");
        clickIdentifyButtonAndDelay();
        String result = driver.findElement(By.name(RESULT_FIELD_NAME)).getAttribute(VALUE);

        //assert
        assertEquals(INVALID_INPUT, result);
    }

    @Test
    public void useCaseTestAfterFixTypo() throws Exception {
        //arrange

        //act
        field1.sendKeys("a");
        field2.sendKeys("5");
        field3.sendKeys("10");
        clickIdentifyButtonAndDelay();
        clickContinueButtonAndDelay();
        field1 = driver.findElement(By.name("side1"));
        field1.sendKeys("10");
        clickIdentifyButtonAndDelay();

        String result = driver.findElement(By.name(RESULT_FIELD_NAME)).getAttribute(VALUE);

        //assert
        assertEquals(EQUILATERAL, result);
    }





    private void assertExistsElementWithName(String elementName) {
        try {
            driver.findElement(By.name(elementName));
        } catch (NoSuchElementException e) {
            fail("Expected that element with name would exist: " + elementName);
        }
    }


    private void clickIdentifyButtonAndDelay() throws InterruptedException {
        WebElement identifyButton = driver.findElement(By.name(IDENTIFY_BUTTON_NAME));
        identifyButton.click();
        Thread.sleep(delay);
        new WebDriverWait(driver, 10000)
                .until(ExpectedConditions.presenceOfElementLocated(By.name(CONTINUE_BUTTON_NAME)));
    }

    private void clickContinueButtonAndDelay() throws InterruptedException {
        WebElement continueButton = driver.findElement(By.name(CONTINUE_BUTTON_NAME));
        continueButton.click();
        Thread.sleep(delay);
        new WebDriverWait(driver, 10000)
                .until(ExpectedConditions.presenceOfElementLocated(By.name(IDENTIFY_BUTTON_NAME)));
    }


    private boolean isOnResultsPage() {
        try {
            driver.findElement(By.name(CONTINUE_BUTTON_NAME));
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isOnInputPage() {
        try {
            driver.findElement(By.name(IDENTIFY_BUTTON_NAME));
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }
}
