package de.tbelmega.assignment2;

import org.openqa.selenium.By;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;

/**
 * Created by Thiemo on 28.04.2016.
 */
public class Part3_FeatureTest extends AbstractAdventureTripsTest {


    @DataProvider
    public static Object[][] provideFeatureTestData(){
        return new Object[][] {
                {"T3.1", INPUT_ERROR, null, "", false, NO_MEMBERSHIP},
                {"T3.2", DISALLOWED_AGE, null, "1", true, LESS_THAN_1_YEAR},
                {"T3.3", null, "0%", "18", false, NO_MEMBERSHIP},
                {"T3.4", null, "5%", "56", true, LESS_THAN_1_YEAR},
                {"T3.5", null, "10%", "26", true, LESS_THAN_5_YEARS},
                {"T3.6", null, "15%", "18", true, LESS_THAN_5_YEARS},
                {"T3.7", null, "25%", "18", true, MORE_THAN_5_YEARS},
                {"T3.10", null, "0%", "18", false, NO_MEMBERSHIP},
                {"T3.11", null, "5%", "18", true, LESS_THAN_1_YEAR},
                {"T3.12", null, "15%", "18", true, LESS_THAN_5_YEARS},
                {"T3.13", null, "25%", "18", true, MORE_THAN_5_YEARS},
                {"T3.14", null, "10%", "26", true, MORE_THAN_5_YEARS}
        };
    }

    @Test(dataProvider = "provideFeatureTestData", timeOut = 5000)
    public void test_3_Feature(String testCase, String expectedResult, String expectedDiscount,
                               String age, boolean member, String years) throws Exception {
        //arrange
        clickDetails();

        //act
        driver.findElement(By.id(ID_AGE)).sendKeys(age);
        if (member) selectMembershipCheckbox();
        else unselectMembershipCheckbox();
        selectMembership(years);

        clickSubmit();

        //assert
        if (expectedResult != null)
            assertEquals(expectedResult, driver.findElement(By.id(ID_RESULT)).getText());
        if (expectedDiscount != null)
            assertEquals(expectedDiscount, driver.findElement(By.id(ID_DISCOUNT)).getText());
    }
}
