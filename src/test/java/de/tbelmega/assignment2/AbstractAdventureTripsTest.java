package de.tbelmega.assignment2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import static org.testng.AssertJUnit.assertEquals;

/**
 * Created by Thiemo on 28.04.2016.
 */
public class AbstractAdventureTripsTest {

    public static final String CHARACTER_BACKSPACE = "\u0008";

    public static final String TITLE_MAIN_PAGE = "Adventure Trip Discounts";
    public static final String TITLE_RESULT_PAGE = "Discount Details";
    public static final String TITLE_INPUT_PAGE = "Enter Applicant Details";

    public static final String ID_CONTINUE = "continue";
    public static final String ID_RETURN = "return";
    public static final String ID_DETAILS = "details";
    public static final String ID_AGE = "age";
    public static final String ID_MEMBER = "member";
    public static final String ID_YEARS = "years";
    public static final String ID_SUBMIT_BUTTON = "submitButton";
    public static final String ID_DISCOUNT = "discount";
    public static final String ID_RESULT = "result";

    public static final String DISALLOWED_AGE = "Sorry, but you must be between 18 and 65 to attend Adventure Trips events.";
    public static final String INPUT_ERROR = "There is an error in the details you have provided.";

    public static final String ATTRIBUTE_VALUE = "value";
    public static final String LESS_THAN_1_YEAR = "Less than 1 year";
    public static final String LESS_THAN_5_YEARS = "Less than 5 years";
    public static final String MORE_THAN_5_YEARS = "More than 5 years";
    public static final String NO_MEMBERSHIP = "Not a member";

    static WebDriver driver;
    static Wait<WebDriver> wait;
    public static final String URL ="http://webcourse.cs.nuim.ie/~sbrown/cs608/discounter.html";


    @BeforeSuite
    public static void setUpBeforeClass() throws Exception {
        // setup selenium test
        driver = new FirefoxDriver();
        wait = new WebDriverWait(driver, 30);
    }

    @AfterSuite
    public static void tearDownAfterClass() throws Exception {
        driver.close();
    }


    @BeforeMethod
    public void setUp() throws Exception {
        driver.get(URL);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id(ID_DETAILS)));

        assertEquals("Tests need to start on the main screen", TITLE_MAIN_PAGE, driver.getTitle());
    }

    /////////////////////////
    // UTIL METHODS
    ////////////////////////


    protected void clickDetails() {
        driver.findElement(By.id(ID_DETAILS)).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id(ID_AGE)));
    }

    protected void clickContinueAndWaitForMainPageContent() {
        driver.findElement(By.id(ID_CONTINUE)).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id(ID_DETAILS)));
    }

    protected void clickContinueAndWaitForDetailsPageContent() {
        driver.findElement(By.id(ID_CONTINUE)).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id(ID_RETURN)));
    }

    protected void clickSubmit() {
        driver.findElement(By.id(ID_SUBMIT_BUTTON)).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id(ID_CONTINUE)));
    }

    protected void clickReturn() {
        driver.findElement(By.id(ID_RETURN)).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id(ID_DETAILS)));
    }

    protected String getSelectedMembership() {
        return new Select(driver.findElement(By.id(ID_YEARS))).getFirstSelectedOption().getText();
    }

    protected void selectMembership(String text) {
        new Select(driver.findElement(By.id(ID_YEARS))).selectByVisibleText(text);
    }

    protected void selectMembershipCheckbox() {
        if (!driver.findElement(By.id(ID_MEMBER)).isSelected())
            driver.findElement(By.id(ID_MEMBER)).click();
    }

    protected void unselectMembershipCheckbox() {
        if (driver.findElement(By.id(ID_MEMBER)).isSelected())
            driver.findElement(By.id(ID_MEMBER)).click();
    }


    protected String getValueOfAgeTextField() {
        return driver.findElement(By.id(ID_AGE)).getAttribute(ATTRIBUTE_VALUE);
    }
}
