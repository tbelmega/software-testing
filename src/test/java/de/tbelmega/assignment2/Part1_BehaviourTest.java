package de.tbelmega.assignment2;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertTrue;
import static org.testng.AssertJUnit.assertEquals;

/**
 * Created by Thiemo on 28.04.2016.
 */
public class Part1_BehaviourTest extends AbstractAdventureTripsTest {


    @Test(timeOut = 5000)
    public void test_1_1_ComponentBehaviour() throws Exception {
        clickDetails();

        driver.findElement(By.id(ID_AGE)).sendKeys("42");
        assertEquals("42", getValueOfAgeTextField());

        driver.findElement(By.id(ID_AGE)).sendKeys(CHARACTER_BACKSPACE);
        assertEquals("4", getValueOfAgeTextField());

        selectMembershipCheckbox();
        assertTrue(driver.findElement(By.id(ID_MEMBER)).isSelected());

        unselectMembershipCheckbox();
        assertFalse(driver.findElement(By.id(ID_MEMBER)).isSelected());

        selectMembership(LESS_THAN_1_YEAR);
        assertEquals(LESS_THAN_1_YEAR, getSelectedMembership());
    }


}
