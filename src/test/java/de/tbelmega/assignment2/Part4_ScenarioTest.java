package de.tbelmega.assignment2;

import org.openqa.selenium.By;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertTrue;

/**
 * Created by Thiemo on 28.04.2016.
 */
public class Part4_ScenarioTest extends AbstractAdventureTripsTest {

    @Test(timeOut = 5000)
    public void test_4_1_ScenarioSuccessfulNoMember() throws Exception {
        clickDetails();
        assertEquals(TITLE_INPUT_PAGE,  driver.getTitle());

        driver.findElement(By.id(ID_AGE)).sendKeys("20");

        clickSubmit();
        assertEquals(TITLE_RESULT_PAGE,  driver.getTitle());
        assertEquals("0%", driver.findElement(By.id(ID_DISCOUNT)).getText());

        clickContinueAndWaitForMainPageContent();
        assertEquals(TITLE_MAIN_PAGE, driver.getTitle());
    }

    @Test(timeOut = 5000)
    public void test_4_2_ScenarioSuccessfulMember() throws Exception {
        clickDetails();
        assertEquals(TITLE_INPUT_PAGE,  driver.getTitle());

        driver.findElement(By.id(ID_AGE)).sendKeys("20");
        selectMembershipCheckbox();
        selectMembership(MORE_THAN_5_YEARS);

        clickSubmit();
        assertEquals(TITLE_RESULT_PAGE,  driver.getTitle());
        assertTrue(driver.findElement(By.id(ID_DISCOUNT)).isDisplayed()); //Scenario does not require a specific value

        clickContinueAndWaitForMainPageContent();
        assertEquals(TITLE_MAIN_PAGE, driver.getTitle());
    }

    @Test(timeOut = 5000)
    public void test_4_3_ScenarioDisallowedAge() throws Exception {
        clickDetails();
        assertEquals(TITLE_INPUT_PAGE,  driver.getTitle());

        driver.findElement(By.id(ID_AGE)).sendKeys("16");

        clickSubmit();
        assertEquals(TITLE_RESULT_PAGE,  driver.getTitle());
        assertEquals(DISALLOWED_AGE, driver.findElement(By.id(ID_RESULT)).getText());

        clickContinueAndWaitForMainPageContent();
        assertEquals(TITLE_MAIN_PAGE, driver.getTitle());
    }

    @Test(timeOut = 5000)
    public void test_4_4_ScenarioInvalidAge() throws Exception {
        clickDetails();
        assertEquals(TITLE_INPUT_PAGE,  driver.getTitle());

        driver.findElement(By.id(ID_AGE)).sendKeys("2r");

        clickSubmit();
        assertEquals(TITLE_RESULT_PAGE,  driver.getTitle());
        assertEquals(INPUT_ERROR, driver.findElement(By.id(ID_RESULT)).getText());

        clickContinueAndWaitForDetailsPageContent();
        assertEquals(TITLE_INPUT_PAGE, driver.getTitle());
    }

    @Test(timeOut = 5000)
    public void test_4_5_ScenarioInconsistentData() throws Exception {
        clickDetails();
        assertEquals(TITLE_INPUT_PAGE,  driver.getTitle());

        driver.findElement(By.id(ID_AGE)).sendKeys("20");
        selectMembershipCheckbox();

        clickSubmit();
        assertEquals(TITLE_RESULT_PAGE,  driver.getTitle());
        assertEquals(INPUT_ERROR, driver.findElement(By.id(ID_RESULT)).getText());

        clickContinueAndWaitForDetailsPageContent();
        assertEquals(TITLE_INPUT_PAGE, driver.getTitle());
    }
}
