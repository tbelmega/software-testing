package de.tbelmega.assignment2;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.*;

/**
 * Created by Thiemo on 28.04.2016.
 */
public class Part2_NavigationTest extends AbstractAdventureTripsTest {


    @Test(timeOut = 5000)
    public void test_2_1_Navigation() throws Exception {
        clickDetails();
        assertEquals(TITLE_INPUT_PAGE,  driver.getTitle());

        driver.findElement(By.id(ID_AGE)).sendKeys("7");
        assertEquals(TITLE_INPUT_PAGE,  driver.getTitle());

        clickSubmit();
        assertEquals(TITLE_RESULT_PAGE,  driver.getTitle());

        clickContinueAndWaitForMainPageContent();
        assertEquals("Navigation test case should end on Main Page", TITLE_MAIN_PAGE, driver.getTitle());
    }

    @Test(timeOut = 5000)
    public void test_2_2_Navigation() throws Exception {
        clickDetails();
        assertEquals(TITLE_INPUT_PAGE,  driver.getTitle());

        clickSubmit();
        assertEquals(TITLE_RESULT_PAGE,  driver.getTitle());

        clickContinueAndWaitForDetailsPageContent();
        assertEquals(TITLE_INPUT_PAGE,  driver.getTitle());

        clickReturn();
        assertEquals("Navigation test case should end on Main Page", TITLE_MAIN_PAGE, driver.getTitle());
    }




}
